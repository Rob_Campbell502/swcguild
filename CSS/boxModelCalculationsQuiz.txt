
#div1 {
	height: 150px;
	width: 400px;
	margin: 20px;
	border: 1px solid red;
	padding: 10px;
{


Total Height:
20px (margin-top) + 1px (border-top) + 10px (padding top) + 150px (height of content) + 10px (padding-bottom) + 1px (border bottom) + 20px (margin bottom) = 212px

Total Width:
20px (margin left) + 1px (border-left) + 10px (padding-left) + 400px (width of content) + 10px (padding-right) + 1px (border right) + 20px (margin right) = 462px

Browser Calculated Height:
1px (border-top) + 10px (padding-top) + 150px (height of content) + 10px (padding-bottom) + 1px (border-bottom) = 172px

Browser Calculated Width:
1px (border-left) + 10px (padding-left) + 400px (width of content) + 10px (padding-right) + 1px (border-right) = 422px